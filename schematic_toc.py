# Script to create table of contents for KiCAD
# Author: Alex Papanicolaou
# Takes in main schematic file 
# Runs script to keep track of the different associated sheets

# Future Updates --------------------------------------------------------------

from sys import argv
import os.path
import subprocess #to execute terminal commands

# Functions -------------------------------------------------------------------

# Main Program ---------------------------------------------------------------- 

script_name, file_name = argv

# Read every reference of $Sheet and print the files associated
sheet_names = []
sheet_names.append(file_name)
count=1
count_file_name=10
schem_file = open(file_name, 'r')

# Table of contents string
toc = []

# find each instance of sheet and get the filename
for line_num in iter(schem_file):
	count_file_name+=1
	if(line_num[:5] == "Title"):
		file_name_line = line_num.split(" ")
		toc.append(file_name_line[1][1:-1]) # The main title
	if(line_num[:6] == "$Sheet"):
		count+=1
		count_file_name=0
	if(count_file_name==4):
		file_name_line = line_num.split(" ")
		sheet_names.append(file_name_line[1][1:-1])
# close the main sheet file
schem_file.close()

# Define arrays containing sheet number and sheet file name
sheet_array = [[] for i in xrange(count)]
for sheet in range(count):
	sheet_array[sheet].append(sheet+1)
	sheet_array[sheet].append(sheet_names[sheet])

# loop through and get the titles in each page
for sheet in range(len(sheet_array)):
	sheet_file = open(sheet_array[sheet][1], 'r')
	for line_num in iter(sheet_file):
		if(line_num[:5] == "Title"):
			file_name_line = line_num.split(" ")
			toc.append(file_name_line[1][1:-1])
	sheet_file.close()

# create reference file to place if doesn't exist
if(os.path.isfile(file_name[:-4]+".toc") == False):
	label_ref_file = open(file_name[:-4]+".toc",'w')
	# write the table of contents
	toc_init = "Text Notes 600 7700 0    80   ~ 0\n"
	label_ref_file.write(toc_init)
	toc_title = toc[0] + "\\n\\n"
	toc_index = 1
	for toc_item in iter(toc):
		toc_line = str(toc_index) + ": " + toc_item
		label_ref_file.write(toc_line + "\\n")
		toc_index+=1
	label_ref_file.close()

	# read sch into buffer
	schem_file_buf = open(file_name, 'r')
	schem_contents = schem_file_buf.readlines()
	schem_contents = schem_contents[:-1]
	schem_file_buf.close()

	# open again and write to the file...
	sheet_file_write = open(file_name, 'w')
	sheet_file_write.writelines(schem_contents)
	sheet_file_write.write(toc_init)
	toc_index_2 = 1
	for toc_item in iter(toc):
		toc_line = str(toc_index_2) + ": " + toc_item
		label_ref_file.write(toc_line + "\\n")
		toc_index_2+=1
	sheet_file_write.write("$EndSCHEMATC\n")
	sheet_file_write.close()