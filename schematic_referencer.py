# Script to reference labels on different schematic sheets for KiCAD
# Author: Alex Papanicolaou
# Takes in main schematic file 
# Runs script to keep track of the different associated sheets

# Future Updates --------------------------------------------------------------

from sys import argv
import os.path
import subprocess #to execute terminal commands

# Functions -------------------------------------------------------------------

# Main Program ---------------------------------------------------------------- 

script_name, file_name, command = argv

# Read every reference of $Sheet and print the files associated
sheet_names = []
sheet_names.append(file_name)
count=1
count_file_name=10
schem_file = open(file_name, 'r')
# find each instance of sheet and get the filename
for line_num in iter(schem_file):
	count_file_name+=1
	if(line_num[:6] == "$Sheet"):
		count+=1
		count_file_name=0
	if(count_file_name==4):
		file_name_line = line_num.split(" ")
		sheet_names.append(file_name_line[1][1:-1])
# close the main sheet file
schem_file.close()

# Define arrays containing sheet number and sheet file name
sheet_array = [[] for i in xrange(count)]
for sheet in range(count):
	sheet_array[sheet].append(sheet+1)
	sheet_array[sheet].append(sheet_names[sheet])

print "\nThe following lists the schematic sheet file names and their page index:\n"

for sheet in range(len(sheet_array)):
	print("%d: Sheet %d, %s" % (sheet,sheet_array[sheet][0],sheet_array[sheet][1]))

#nprint "\nThe following lists the global labels present in each sheet:"

# define an array containing global labels and their page numbers
glabel_array = []

# loop through and get the glabels in each page
for sheet in range(len(sheet_array)):
	sheet_file = open(sheet_array[sheet][1], 'r')
	# print("\n%s Labels:\n" % (sheet_array[sheet][1]))

	# define more counting variables for each sheet
	count_glabel = 3
	count_glabel_unique = 0

	# loop through the file for each label
	for line_num in iter(sheet_file):
		count_glabel+=1
		if(line_num[:11] == "Text GLabel"):
			count_glabel=0
		if(count_glabel==1):
			if line_num[:-1] in sheet_array[sheet]:
				continue
			else:
				sheet_array[sheet].append(line_num[:-1])
				# print("%s" % (line_num[:-1]))
				if line_num[:-1] in glabel_array:
					continue
				else:
					count_glabel_unique+=1
					glabel_array.append(line_num[:-1])

	sheet_file.close()

# print "\nThe following lists the unique global labels present the schematic:\n"

# define array containing glabels and indices
annotation_array = [[] for i in xrange(len(glabel_array))]

for item in range(len(glabel_array)):
	# print("%d, %s" % (item,glabel_array[item]))
	annotation_array[item].append(glabel_array[item])

	# iterate through sheets again and add page indices to glabel_array
	for sheet in range(len(sheet_array)):
		if glabel_array[item] in sheet_array[sheet]:
			annotation_array[item].append(sheet_array[sheet][0])

print "\nThe following lists the unique global labels with the sheet references:\n"

for item in range(len(annotation_array)):
	print("%d, %s on pages: %r" % (item, annotation_array[item][0], annotation_array[item][1:]))

# Writing Text References to File ---------------------------------------------

# Structure of a GLabel definition:
# Text GLabel posX posY orientation size Input/Output/BiDi ~ bold/reg
# <GLabel name>

# Structure of Notes definition:
# Text Notes posX posY orientation size ~ bold/reg
# <Notes text>

# posX/Y/size in mils, orientation 0: orig on right, 1: on bottom, 2: on left

# create reference file to place if doesn't exist
if(os.path.isfile(file_name[:-4]+".ref") == False):
	label_ref_file = open(file_name[:-4]+".ref",'w')

	for sheet in range(len(sheet_array)):
		sheet_file = open(sheet_array[sheet][1], 'r')
		sheet_number = sheet_array[sheet][0]

		# or just find where it says $EndSCHEMATC and replace with Text Notes and add the
		# end schematic after
		sheet_contents = sheet_file.readlines()
		sheet_contents = sheet_contents[:-1]
		sheet_file.close()

		# open again and write to the file...
		sheet_file_write = open(sheet_array[sheet][1], 'w')
		sheet_file_write.writelines(sheet_contents)

		# insert the references
		for item in sheet_contents:
			if(item[:11] == "Text GLabel"):
				# get item index, extract info, get item + 1 index
				item_array = item.split(" ")
				item_array = [x for x in item_array if x != '']
				posX = int(item_array[2])
				posY = int(item_array[3])
				orientation = int(item_array[4])
				size = int(item_array[5])
				label_name = sheet_contents[sheet_contents.index(item)+1]

				# get corresponding label from annotation_array
				for label in annotation_array:
					#print(str(label) + ", " + label_name)
					if(label[0]+'\n' == label_name):
						# print "yes!\n"
						posX_new = 0
						temp_label = label
						# write stuff here
						if orientation == 0:
							posX_new = posX - size*(1+len(label_name)) + 0.5*size
							orientation = 2
						elif orientation == 2:
							posX_new = posX + size*(1+len(label_name)) - 0.5*size
							orientation = 0
						
						
						#temp_label.remove(label_name[:-1])
						#temp_label.remove(sheet_number)

						if len(temp_label[1:]) != 0:
							notes_line = "Text Notes %d %d %d    30   ~ 0\n" % (posX_new,posY,orientation)
							sheet_file_write.write(notes_line)
						
							ref_line = str(temp_label[1:]).strip('[]')
							sheet_file_write.write(ref_line+"\n")
							# add to ref file too
							label_ref_file.write(ref_line+"\n")
						break

		sheet_file_write.write("$EndSCHEMATC\n")
		sheet_file_write.close()

	label_ref_file.close()

else:
	# open the file and delete current references and redo the operation
	label_ref_file = open(file_name[:-4]+".ref",'r')
	label_ref_file_buffer = label_ref_file.readlines()
	label_ref_file.close()

	# open file again for writing...
	label_ref_write = open(file_name[:-4]+".ref",'w')

	for sheet in range(len(sheet_array)):
		sheet_file = open(sheet_array[sheet][1], 'r')
		sheet_file_buffer = sheet_file.readlines()
		sheet_file_buffer = sheet_file_buffer[:-1]
		sheet_file.close()

		# reference file contains the label page indices only
		# delete references from file
		for item in range(len(label_ref_file_buffer)):
			if label_ref_file_buffer[item] in sheet_file_buffer:
				item_index = sheet_file_buffer.index(label_ref_file_buffer[item])
				del sheet_file_buffer[item_index]
				del sheet_file_buffer[item_index-1]

		# if optional argument = remove then just close the file and end here
		if(command == "remove"):
			print("Removed Labels!\n")

		else:
			# add new references at end of file before $EndSCHEMATC line
			sheet_number = sheet_array[sheet][0]
			for item in sheet_file_buffer:
				if(item[:11] == "Text GLabel"):
					# get item index, extract info, get item + 1 index
					item_array = item.split(" ")
					item_array = [x for x in item_array if x != '']
					posX = int(item_array[2])
					posY = int(item_array[3])
					orientation = int(item_array[4])
					size = int(item_array[5])
					label_name = sheet_file_buffer[sheet_file_buffer.index(item)+1]

					# get corresponding label from annotation_array
					for label in annotation_array:
						if(label[0]+'\n' == label_name):
							posX_new = 0
							# write stuff here
							if orientation == 0:
								posX_new = posX - size*(1+len(label_name)) + 0.5*size
								orientation = 2
							elif orientation == 2:
								posX_new = posX + size*(1+len(label_name)) - 0.5*size
								orientation = 0
							
							temp_label = label
							#temp_label.remove(label_name)
							#temp_label.remove(sheet_number)

							if len(temp_label[1:]) != 0:
								notes_line = "Text Notes %d %d %d    30   ~ 0\n" % (posX_new,posY,orientation)
								sheet_file_buffer.append(notes_line)
							
								ref_line = str(temp_label[1:]).strip('[]')
								sheet_file_buffer.append(ref_line+"\n")
								# add to ref file too
								label_ref_write.write(ref_line+"\n")

		sheet_file_write = open(sheet_array[sheet][1], 'w')
		sheet_file_write.writelines(sheet_file_buffer)
		sheet_file_write.write("$EndSCHEMATC\n")
		sheet_file_write.close()

	label_ref_write.close()

	subprocess.call(['rm', file_name[:-4]+".ref"])

	print("Done!\n")