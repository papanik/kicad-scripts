# Script to parse pin description tables into consistent format for KiCAD
# Author: Alex Papanicolaou
# Takes in csv file containing unformatted datasheet information
# Runs script producing csv file containing columns:
# "pin name, pin number, pin direction"
# Creates a file containing a new KiCAD library entry 
# User must manually create component body and rearrange pins
# Time is saved from manually entering all pin information 

# Future Updates --------------------------------------------------------------
# - find a proper way to arrange big components into many parts automatically

from sys import argv
import csv
import subprocess #to execute terminal commands

# Functions -------------------------------------------------------------------

# Main Program ---------------------------------------------------------------- 

print """Library Component Pre-formatting tool for KiCAD
-------------------------------------------------------------------------------
Takes in a csv input file

Arguments as follows: 
script_name.py, raw_pin_file_name.csv, output_file_name.txt 
"""

script_name, raw_pin_file_name, output_file_name  = argv

# Should check to make sure extension .csv is used
if(raw_pin_file_name[-3:] == "csv"):
	raw_pin_file_name_path = raw_pin_file_name
else:
	raw_pin_file_name_path = raw_pin_file_name + ".csv"


entry_parameters = raw_input("""Please enter some information about the symbol:
symbol_name, prefix

Note 1: separate fields using commas
Note 2: prefix is the first character of the reference designator for the symbol
      (U, R, C, J)

""").split(',')

# Create and open the file to be written
symbol_file = open(output_file_name, 'w')

# Write opening lines for the symbol definition
symbol_file.write("# %s\n#\n" % entry_parameters[0])
symbol_file.write("DEF %s %s 0 40 Y Y 1 F N\n" % (entry_parameters[0], entry_parameters[1].strip()))
symbol_file.write("F0 \"%s\" 0 1000 60 H V C CNN\n" % entry_parameters[1].strip())
symbol_file.write("F1 \"%s\" 0 -1000 60 H V C CNN\n" % entry_parameters[0])
symbol_file.write("F2 \"\" -900 2150 60 H V C CNN\n")
symbol_file.write("F3 \"\" -900 2150 60 H V C CNN\n")
symbol_file.write("DRAW\n")

# Write the pin descriptions
# Read pin files and write to the text file
subprocess.call(['python', 'library_synthesis_pin_table.py', raw_pin_file_name_path, 'outputFile.csv'])

with open('outputFile.csv', 'rU') as input_file_object:
	pin_file = csv.reader(input_file_object, skipinitialspace=True)
	pin_x_pos=-800
	pin_y_pos=5000
	index_count=1
	for row in pin_file:
		if any(row):
			if(len(row)==2):
				symbol_file.write("X %s %s %d %d 100 L 50 50 1 1 I\n" % (row[0],row[1],pin_x_pos,pin_y_pos))
			else:
				symbol_file.write("X %s %s %d %d 100 L 50 50 1 1 %s\n" % (row[0],row[1],pin_x_pos,pin_y_pos,row[2]))
			pin_y_pos-=100
			if(index_count%100==0):
				pin_x_pos=pin_x_pos+800
				pin_y_pos=5000
			index_count+=1

# Write the closing lines for the symbol definition
symbol_file.write("ENDDRAW\nENDDEF\n")
symbol_file.write("#\n")

# Close the file
symbol_file.close()
# Remove intermediate pin files
subprocess.call(['rm', 'outputFile.csv'])