# Script to import components grouped together by sheet for KiCAD
# Author: Alex Papanicolaou
# Takes in main schematic file 
# Runs script to keep track of the positions of each component per sheet
# Modifies imported netlist to group the above components (common origin)
# Should only be used on initial import
# - import netlist from pcbnew and save
# - close pcbnew, run script

# Future Updates --------------------------------------------------------------
# On every netlist update, only modify the new component positions and
# place away from board outline

from sys import argv
import os.path
import subprocess #to execute terminal commands

# Functions -------------------------------------------------------------------

# Main Program ---------------------------------------------------------------- 

script_name, file_name_sch, file_name_brd = argv

# Read every reference of $Sheet and print the files associated
sheet_names = []
count=1
count_file_name=10
schem_file = open(file_name, 'r')
# find each instance of sheet and get the filename
for line_num in iter(schem_file):
	count_file_name+=1
	if(line_num[:6] == "$Sheet"):
		count+=1
		count_file_name=0
	if(count_file_name==4):
		file_name_line = line_num.split(" ")
		sheet_names.append(file_name_line[1][1:-1])
# close the main sheet file
schem_file.close()

# loop through and get the components in each page (except #PWR refdes)
for sheet in range(len(sheet_names)):
	sheet_file = open(sheet_names[sheet], 'r')
	

# Create component list file to store components that have been previously 
# imported