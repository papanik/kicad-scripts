from sys import argv
import csv
import numpy as np
import matplotlib.pyplot as plt

# Open/Create the file object, name it, write permissions
input_data_table_row = []
input_file_name_path = ""

if(len(argv) == 2):
	# Should check to make sure extension .csv is used
	if(argv[1][-3:] == "csv"):
		input_file_name_path = argv[1]
	else:
		input_file_name_path = argv[1] + ".csv"

	# Open the file
	with open(input_file_name_path, 'rU') as input_file_object:
		input_file = csv.reader(input_file_object, skipinitialspace=True)
		for row in input_file:
			if any(row):
				input_data_table_row.append(row)

# Plot the function
fig, ax1 = plt.subplots()
ax1.set_ylim([0, 5])
ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Voltage (V)')
ax1.set_title("Gas Gauge Battery Voltage vs Time")

# add to the X, Y arrays
x = []
y = []
for row in input_data_table_row:
	x.append(float(row[0]))
	y.append(float(row[1]))
plt.plot(x,y,'ro-')
plt.plot(x[8],y[8],'bo-',x[9],y[9],'bo-')


# Add nice grid-lines and show the graph
plt.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
plt.show()