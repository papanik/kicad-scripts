# Script to parse pin description tables into consistent format for KiCAD
# Author: Alex Papanicolaou
# Takes in csv file containing unformatted component pin information
# (copied from datasheet pin mapping tables)
# Places the data in a simple format to enter into a .lib file
# Still need to manually arrange the pins onto a symbol body

# Future Updates --------------------------------------------------------------
# - take in an xlsx excel file to remove one less step in pre-data formatting

from sys import argv
import csv
import subprocess #to execute terminal commands

# Functions -------------------------------------------------------------------

# Main Program ---------------------------------------------------------------- 

print """Library Component Pre-formatting tool for KiCAD
-------------------------------------------------------------------------------
Takes in a csv input file

Arguments as follows: 
script_name.py, input_file_name.csv, output_file_name.csv
"""

script_name, input_file_name, output_file_name = argv

# Open/Create the file object, name it, write permissions
input_file_name_path = input_file_name
input_data_table_row = []

# Should check to make sure extension .csv is used
if(input_file_name[-3:] == "csv"):
	input_file_name_path = input_file_name
else:
	input_file_name_path = input_file_name + ".csv"

# Open the file
with open(input_file_name_path, 'rU') as input_file_object:
	input_file = csv.reader(input_file_object, skipinitialspace=True)
	for row in input_file:
		if any(row):
			input_data_table_row.append(row)

# Output file csv format: Pin name, pin number, input/output (default input)
# Should check to make sure extension .csv is used
out_format = raw_input("""Enter column index of pin name, pin number, I/O as:
name, number, io

Note: first column starts at index 0. If io not used, mark as n/a

""").split(',')
pin_name = int(out_format[0])
pin_number = int(out_format[1])

output_file_name_path = output_file_name

if(output_file_name[-3:] == "csv"):
	output_file_name_path = output_file_name
else:
	output_file_name_path = output_file_name + ".csv"

with open(output_file_name_path, 'w') as output_file_object:
	output_file = csv.writer(output_file_object)
	row_index=0
	for row in input_data_table_row:
		if(out_format[2]=="n/a"):
			pin_number_list=input_data_table_row[row_index][pin_number].split(',')
			item_index=0
			for item in pin_number_list:
				output_file.writerow([input_data_table_row[row_index][pin_name].strip()]+[pin_number_list[item_index].strip()])
				item_index+=1
		else:
			pin_direction = int(out_format[2])
			pin_number_list=input_data_table_row[row_index][pin_number].split(',')
			item_index=0
			for item in pin_number_list:
				output_file.writerow([input_data_table_row[row_index][pin_name].strip()]+[pin_number_list[item_index].strip()]+[input_data_table_row[row_index][pin_direction].strip()])
				item_index+=1
		row_index+=1

# Delete input_file to clean up
subprocess.call(['rm', input_file_name_path])
