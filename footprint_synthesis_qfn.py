# Script to create BGA packages for KiCAD
# Author: Alex Papanicolaou
# Takes in parameters as input (name, package dimensions, mask opening, etc)
# Writes to file with required format
# In future, parse image from datasheet and do things automatically

from sys import argv

# Write name info + rest of line 1 
# Save file path in ../KICAD/footprints/bga_packages.pretty/<name>.kicad_mod
# Write clearance information on lines 2, 3
# Write ref and value infos next
# Loop and write each pad information

# Functions -------------------------------------------------------------------

# Main Program ---------------------------------------------------------------- 

print """QFN footprint synthesis tool for KiCAD
-------------------------------------------------------------------------------
Note: all dimensions are in mm. Uses clearance and solder expansion of 0.076mm

Arguments as follows: 
script_name.py, module_name, D, E, D2, E2, e, b, L, total_pads
"""

script_name, module_name, D, E, D2, E2, e, b, L, total_pads = argv

# Are matrix_sizes even? if so, adjust pad around 0, 0 accordingly
pads = int(total_pads)
start_pos_x = 0.5*float(e)*(1 - pads/4)
start_pos_y = 0.5*float(e)*(1 - pads/4)

# Open/Create the file object, name it, write permissions
module_filename = argv[1] + ".kicad_mod"
module_file = open(module_filename, 'w')

# Start writing to the file
module_file.write("(module %s (layer F.Cu) (tedit 5695AEF4)\n" % argv[1])
module_file.write("  (solder_mask_margin 0.076)\n  (clearance 0.076)\n")
module_file.write("  (fp_text reference REF** (at 0 0) (layer F.SilkS) hide\n")
module_file.write("    (effects (font (size 0.5 0.5) (thickness 0.125)))\n  )\n")
module_file.write("  (fp_text value %s (at 0 %f) (layer F.SilkS) hide\n" % (module_name, (-1*(float(E)*0.5)-0.75)))
module_file.write("    (effects (font (size 1 1) (thickness 0.15)))\n  )\n")

# Now, the chip outline
h_D = float(D)*0.5
h_E = float(E)*0.5

module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-h_D,-h_E,start_pos_x-1.5*float(b),-h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-1*(start_pos_x-1.5*float(b)),-h_E,h_D,-h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (h_D,-h_E,h_D,start_pos_y-1.5*float(b)))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (h_D,-1*(start_pos_y-1.5*float(b)),h_D,h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (h_D,h_E,-1*(start_pos_x-1.5*float(b)),h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (start_pos_x-1.5*float(b),h_E,-h_D,h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-h_D,h_E,-h_D,-1*(start_pos_y-1.5*float(b))))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-h_D,start_pos_y-1.5*float(b),-h_D,-h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-h_D,start_pos_y-1.5*float(b),start_pos_x-1.5*float(b),-h_E))

# Check for electrical pad (EP)
if(float(D2) != 0):
	D_2 = float(D2)
	E_2 = float(E2)
	module_file.write("  (pad EP smd rect (at 0 0) (size %f %f) (layers F.Cu F.Paste F.Mask))\n" % (D_2, E_2))

# Start adding pins 1 to total_pads/4
pad_width = float(b)
pad_length = float(L) + 0.2
for x in range(0, (pads/4)):
	pos_x = -0.5*(float(D) - float(L)) - 0.1
	pad_name = str(x+1)
	y_p = x
	module_file.write("  (pad %s smd rect (at %f %f) (size %f %f) (layers F.Cu F.Paste F.Mask))\n" % (pad_name, pos_x, start_pos_y+float(e)*y_p, pad_length, pad_width))
for x in range((pads/4), (pads/2)):
	pos_y = 0.5*(float(E) - float(L)) + 0.1
	pad_name = str(x+1)
	x_p = x - pads/4
	module_file.write("  (pad %s smd rect (at %f %f) (size %f %f) (layers F.Cu F.Paste F.Mask))\n" % (pad_name, start_pos_x+float(e)*x_p, pos_y, pad_width, pad_length))
for x in range((pads/2), int(pads*0.75)):
	pos_x = 0.5*(float(D) - float(L)) + 0.1
	pad_name = str(x+1)
	y_p = x - pads/2
	module_file.write("  (pad %s smd rect (at %f %f) (size %f %f) (layers F.Cu F.Paste F.Mask))\n" % (pad_name, pos_x, -1*(start_pos_y+float(e)*y_p), pad_length, pad_width))
for x in range(int(pads*0.75), pads):
	pos_y = -0.5*(float(E) - float(L)) - 0.1
	pad_name = str(x+1)
	x_p = x - 3*pads/4
	module_file.write("  (pad %s smd rect (at %f %f) (size %f %f) (layers F.Cu F.Paste F.Mask))\n" % (pad_name, -1*(start_pos_x+float(e)*x_p), pos_y, pad_width, pad_length))

# End module
module_file.write(")")

# Close the file
module_file.close()