from Tkinter import *
class scrollTxtArea:
    def __init__(self,root):
        frame=Frame(root)
        frame.pack()
        self.textPad(frame)
        return
 
    def textPad(self,frame):
        #add a frame and put a text area into it
        textPad=Frame(frame)
        self.text=Label(textPad,height=25,width=40)
         
        # add a vertical scroll bar to the text area
        scroll=Scrollbar(textPad)
        self.text.configure(yscrollcommand=scroll.set)
         
        #pack everything
        self.text.pack(side=LEFT)
        scroll.pack(side=RIGHT,fill=Y)
        textPad.pack(side=TOP)
        return
def main():
    root = Tk()
    foo=scrollTxtArea(root)
    root.title('Gas Gauge Data Stream')
    user_input = raw_input()
    foo.textPad.set(user_input + "\n")
    root.mainloop()

main()