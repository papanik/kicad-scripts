# Script to create BGA packages for KiCAD
# Author: Alex Papanicolaou
# Takes in parameters as input (name, package dimensions, mask opening, etc)
# Writes to file with required format
# In future, parse image from datasheet and do things automatically

# Future Updates --------------------------------------------------------------
# - take ball diameter, make pad per IPC-7351A standard

from sys import argv

# Write name info + rest of line 1 
# Save file path in ../KICAD/footprints/bga_packages.pretty/<name>.kicad_mod
# Write clearance information on lines 2, 3
# Write ref and value infos next
# Loop and write each pad information

# Functions -------------------------------------------------------------------

# Main Program ---------------------------------------------------------------- 

print """BGA footprint synthesis tool for KiCAD
-------------------------------------------------------------------------------
Note: all dimensions are in mm. Uses clearance and solder expansion of 0.076mm

Arguments as follows: 
script_name.py, module_name, D, E, e, b, matrix_size_D, matrix_size_E
"""

script_name, module_name, D, E, e, b, matrix_size_D, matrix_size_E = argv

# Are matrix_sizes even? if so, adjust pad around 0, 0 accordingly
start_pos_x = 0.5*float(e)*(1 - float(matrix_size_D))
start_pos_y = 0.5*float(e)*(1 - float(matrix_size_E))

# Open/Create the file object, name it, write permissions
module_filename = argv[1] + ".kicad_mod"
module_file = open(module_filename, 'w')

# Start writing to the file
module_file.write("(module %s (layer F.Cu) (tedit 5695AEF4)\n" % argv[1])
module_file.write("  (solder_mask_margin 0.076)\n  (clearance 0.076)\n")
module_file.write("  (fp_text reference REF** (at 0 0) (layer F.SilkS) hide\n")
module_file.write("    (effects (font (size 0.5 0.5) (thickness 0.125)))\n  )\n")
module_file.write("  (fp_text value %s (at 0 %f) (layer F.SilkS) hide\n" % (module_name, (-1*(float(E)*0.5)-0.75)))
module_file.write("    (effects (font (size 1 1) (thickness 0.15)))\n  )\n")

# Now, the chip outline
h_D = float(D)*0.5
h_E = float(E)*0.5
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-h_D,-h_E,h_D,-h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (h_D,-h_E,h_D,h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (h_D,h_E,-h_D,h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-h_D,h_E,-h_D,-h_E))
module_file.write("  (fp_line (start %f %f) (end %f %f) (layer F.SilkS) (width 0.15))\n" % (-h_D,((-h_E)+0.5),((-h_D)+0.5),-h_E))

# Loop through and add pads
pad_size = float(b)
pitch = float(e)
letter_index = ["A","B","C","D","E","F","G","H","J","K","L","M","N","P","R","T","U","V","W","Y"]
balls_D = int(matrix_size_D)
balls_E = int(matrix_size_E)
for y in range(0, balls_E):
  for x in range(0, balls_D):
    if y<20:
      pad_name = letter_index[y] + str(x+1)
      module_file.write("  (pad %s smd circle (at %f %f) (size %f %f) (layers F.Cu F.Paste F.Mask))\n" % (pad_name,start_pos_x+pitch*x,start_pos_y+pitch*y,pad_size,pad_size))
    else:
      pad_name = "A" + letter_index[y-20] + str(x+1)
      module_file.write("  (pad %s smd circle (at %f %f) (size %f %f) (layers F.Cu F.Paste F.Mask))\n" % (pad_name,start_pos_x+pitch*x,start_pos_y+pitch*y,pad_size,pad_size))

# End module
module_file.write(")")

# Close the file
module_file.close()